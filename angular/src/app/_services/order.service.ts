import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IOrder } from '../_models/_interfaces/IOrder';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private _http:HttpClient) { }
  private _url =  `${environment.apiUrl}/api/order`
  getOrdersByPageForSpecficUser(userID:string,pageSize:number, pageNumber:number):Observable<IOrder[]>{
    return this._http.get<IOrder[]>(`${this._url}/${userID}/${pageSize}/${pageNumber}`).pipe(catchError((err)=>
    {
      return throwError(err.message ||"Internal Server error contact site adminstarator");
    }));
  }
  makeOrder(orderDetails:IOrder){
    let url = `${environment.apiUrl}/api/order`;
    return this._http.post<IOrder>(url, orderDetails)
            .pipe(catchError((err)=>{
              return throwError(err.message ||"Internal Server error contact site adminstarator");
                }
              ));
  }
}
