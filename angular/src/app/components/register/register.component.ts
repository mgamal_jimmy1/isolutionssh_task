import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { IUser } from 'src/app/_models/_interfaces/IUser';
import { RegisterService } from 'src/app/_services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  genderList = ["male", "female"]
  constructor
    (private formBuilder: FormBuilder,
      private _route: ActivatedRoute,
      private _router: Router,
      private _registerService: RegisterService
    ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      email: ['', Validators.required],
      fullName: ['', Validators.required],
    });
  }

  get formFields() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    let newUser: IUser = {
      id: "",
      userName: this.formFields.username.value,
      passwordHash: this.formFields.password.value,
      confirmPassword: this.formFields.confirmPassword.value,
      email: this.formFields.email.value,
      fullName: this.formFields.fullName.value,
    }
    this._registerService.addNewUser(newUser)
      .pipe(first())
      .subscribe(
        data => {
          this._router.navigate(["login"]);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}
