import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_services/authentication.service';

@Component({
  selector: 'app-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.scss']
})
export class DashboardHeaderComponent implements OnInit {
  siteLang:string = "en";
  public get isUserLoggedIn(){
    return this._authenticationService.isLoggedIn();
  } 
  constructor(private _authenticationService:AuthenticationService) { }

  ngOnInit(): void {
    this.siteLang = localStorage.getItem("lang") || "en"
  }
  logoutUser(){
    this._authenticationService.logout();
  }
  changeLang(lang:string){
    this.siteLang = lang
    localStorage.setItem("lang",lang);
    window.location.reload();
  }
}
