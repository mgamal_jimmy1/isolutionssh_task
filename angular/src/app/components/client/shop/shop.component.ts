import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { error } from 'protractor';
import { IOrder } from 'src/app/_models/_interfaces/IOrder';
import { IProduct } from 'src/app/_models/_interfaces/IProduct';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { OrderService } from 'src/app/_services/order.service';
import { ProductService } from 'src/app/_services/product.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
  hasProducts:boolean = false;
  errorMsg: string;
  productsPerPage: IProduct[];
  pageSize: number = 9;
  productsCount: number;
  currentPageNumber: number = 1;
  numberOfPages: number;
  siteLang:string = "en";
  constructor(
    private _productSevice: ProductService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _authenticationService: AuthenticationService,
    private _orderService:OrderService) {

    this._route.queryParams
      .subscribe(params => {
        this.getSelectedPage(1);

      })
  }

  ngOnInit(): void {
    this.getProductsCount();
    this.siteLang = localStorage.getItem("lang") || "en";
  }

  getProductsPerPage(currentPageNumber: number) {
    this._productSevice.getProductsByPage(this.pageSize, currentPageNumber).subscribe(
      data => {
        this.productsPerPage = data
        this.currentPageNumber = currentPageNumber;
        this.getProductsCount();
        if(data.length != 0)
          this.hasProducts = true;
        else
          this.hasProducts = false;

      },
      error => {
        this.errorMsg = error;
      }
    )
  }

  // pagination
  counter(i: number) {
    return new Array(i);
  }
  getSelectedPage(currentPageNumber: number) {
    this.getProductsPerPage(currentPageNumber);
  }
  // image path
  public createImgPath = (serverPath: string) => {
    return `${environment.apiUrl}/${serverPath}`;
  }

  getProductsCount(categoryId = 0, colorId = 0) {
    this._productSevice.getProductsCount(categoryId, colorId).subscribe(
      data => {
        this.productsCount = data
        this.numberOfPages = Math.ceil(this.productsCount / this.pageSize)
      },
      error => {
        this.errorMsg = error;
      }
    )
  }
  Buy(productId:number){
    console.log("bought");
  }
  isLoggedIn():boolean{
    return this._authenticationService.isLoggedIn();
  }
}
