import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { resetPassword } from 'src/app/_models/_interfaces/resetPassword';
import { RegisterService } from 'src/app/_services/register.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  successMsg = '';
  token:string;
  email:string;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private registerService : RegisterService
  ) { }

  ngOnInit(): void {
    this.token = this.route.snapshot.queryParams['token'];
    this.email = this.route.snapshot.queryParams['email'];
    console.log(this.token);
    this.resetPasswordForm = this.formBuilder.group({
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }
  get formFields() { return this.resetPasswordForm.controls; }

  onSubmit() {
      this.submitted = true;

      if (this.resetPasswordForm.invalid) {
          return;
      }
      this.loading = true;
      let resetPasswordModal:resetPassword = {
        PasswordHash : this.formFields.newPassword.value,
        confirmPassword : this.formFields.confirmPassword.value,
        token : this.token,
        email : this.email
      }

      this.registerService.resetPassword(resetPasswordModal)
          .pipe(first())
          .subscribe(
              data => {
                  this.router.navigate(['login']);
              },
              error => {
                this.router.navigate(['login']);
                  this.loading = false;
              });
  }

}
