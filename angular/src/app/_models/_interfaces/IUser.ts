export interface IUser {
    id:string;
    userName:string;
    passwordHash:string;
    confirmPassword:string;
    email:string;
    fullName:string;
}