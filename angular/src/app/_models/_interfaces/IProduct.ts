export interface IProduct {
  id:number,
  name:string,
  nameAr:string,
  image:string
}