import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent} from './components/login/login.component';
import { AuthGuard } from './_helpers/auth.guard';
import { ProductsComponent } from './components/dashboard/products/products.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ClientComponent } from './components/client/client.component';
import { ShopComponent } from './components/client/shop/shop.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { UserRoles } from './_models/_enums/UserRoles';


const routes: Routes = [
  {path:"register", component: RegisterComponent},
  {path:"login", component: LoginComponent},
  {
    path:"dashboard", 
    component: DashboardComponent,
    canActivate:[AuthGuard],
    canActivateChild:[AuthGuard],
    data:{
      role: UserRoles.Admin
    },
    children:[
      {path:"product",component:ProductsComponent}
    ]
  },
  {
    path:"",
    component:ClientComponent,
    children:[
      {path:"",component:ShopComponent},
      {path:"resetPassword", component: ResetPasswordComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
