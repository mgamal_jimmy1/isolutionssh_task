import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular';
  constructor(translate: TranslateService) {
    translate.setDefaultLang('en');
    const lang = localStorage.getItem("lang")
    translate.use(lang);
    document.documentElement.lang = lang;
}
}
