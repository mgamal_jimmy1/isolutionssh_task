﻿using BL.Interfaces;
using BL.Repositories;
using DAL;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Bases
{
    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext
    {
        #region Common Properties
        private TContext EC_DbContext { get; set; }
        private UserManager<ApplicationUserIdentity> _userManager;
        private RoleManager<IdentityRole> _roleManager;
  

        #endregion

        #region Constructors
        public UnitOfWork(TContext EC_DbContext, UserManager<ApplicationUserIdentity> userManager, RoleManager<IdentityRole> roleManager)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
            this.EC_DbContext = EC_DbContext;//
      

            // Avoid load navigation properties
            //EC_DbContext.Configuration.LazyLoadingEnabled = false;
        }
        #endregion

        #region Methods
        public int Commit()
        {
            return EC_DbContext.SaveChanges();
        }

        public void Dispose()
        {
            EC_DbContext.Dispose();
        }
        #endregion





        private AccountRepository<TContext> account;
        public AccountRepository<TContext> Account
        {
            get
            {
                if (account == null)
                    account = new AccountRepository<TContext>(EC_DbContext,_userManager,_roleManager);
                return account;
            }
        }
        
        private RoleRepository<TContext> role;
        public RoleRepository<TContext> Role
        {
            get
            {
                if (role == null)
                    role = new RoleRepository<TContext>(EC_DbContext, _roleManager);
                return role;
            }
        }

        private UserRepository<TContext> user;
        public UserRepository<TContext> User
        {
            get
            {
                if (user == null)
                    user = new UserRepository<TContext>(EC_DbContext);
                return user;
            }
        }
        private ProductRepository<TContext> product;
        public ProductRepository<TContext> Product
        {
            get
            {
                if (product == null)
                    product = new ProductRepository<TContext>(EC_DbContext);
                return product;
            }
        }
        private OrderRepository<TContext> order;
        public OrderRepository<TContext> Order
        {
            get
            {
                if (order == null)
                    order = new OrderRepository<TContext>(EC_DbContext);
                return order;
            }
        }

    }
}
