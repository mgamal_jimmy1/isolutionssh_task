﻿using AutoMapper;
using BL.Configurations;
using BL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Bases
{
    public class AppServiceBase<TContext> : IDisposable where TContext: DbContext
    {

        #region Vars
        protected IUnitOfWork<TContext> TheUnitOfWork { get; set; }
        protected readonly IMapper Mapper; //MapperConfig.Mapper;
       
        #endregion

        #region CTR
        public AppServiceBase(IUnitOfWork<TContext> theUnitOfWork, IMapper mapper)
        {
            TheUnitOfWork = theUnitOfWork;
            Mapper = mapper;
        }

        public void Dispose()
        {
            TheUnitOfWork.Dispose();
        }
        #endregion
    }
}
