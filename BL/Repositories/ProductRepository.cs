﻿using BL.Bases;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Repositories
{
    public class ProductRepository<TContext>: BaseRepository<Product, TContext> where TContext:DbContext
    {

        private DbContext EC_DbContext;

        public ProductRepository(TContext EC_DbContext) : base(EC_DbContext)
        {
            this.EC_DbContext = EC_DbContext;
        }
        #region CRUB

        public IEnumerable<Product> GetAllProduct()
        {
            return GetAll().ToList();
        }
        public bool CheckProductExists(Product product)
        {
            return GetAny(l => l.ID== product.ID);
        }
        public Product GetProductById(int id)
        {
            var product = DbSet.FirstOrDefault(p => p.ID == id);
            return product;
        }


        #endregion

        public override IEnumerable<Product> GetPageRecords(int pageSize, int pageNumber)
        {
            pageSize = (pageSize <= 0) ? 10 : pageSize;
            pageNumber = (pageNumber < 1) ? 0 : pageNumber - 1;

            var products = DbSet
                .Skip(pageNumber * pageSize).Take(pageSize)
                .ToList();
            return products;
        }
        public int CountProducts()
        {
            return DbSet.Count();
        }
    }
}
