﻿using BL.Bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace BL.Repositories
{
    public class OrderRepository<TContext> : BaseRepository<Order, TContext> where TContext:DbContext
    {
        private TContext EC_DbContext;

        public OrderRepository(TContext EC_DbContext) : base(EC_DbContext)
        {
            this.EC_DbContext = EC_DbContext;
        }
        #region CRUB

        public bool InsertOrder(Order order)
        {
            return Insert(order);
        }
        public void UpdateOrder(Order order)
        {
            Update(order);
        }
        public void DeleteOrder(int id)
        {
            Delete(id);
        }


        #endregion
        public  IEnumerable<Order> GetOrdersForSpeceficUser(string userID)
        {
            return DbSet.Where(o => o.UserId == userID);
        }

    }
}
