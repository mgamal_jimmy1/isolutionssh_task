﻿using AutoMapper;
using BL.DTOs;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Configurations
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            //CreateMap<Product, ProductViewModel>()
            //    //.ForMember(vm => vm.ColorName, vm => vm.MapFrom(m => m.Color.Name))
            //    //.ForMember(vm => vm.CategoryName, vm => vm.MapFrom(m => m.Category.Name))
            //    .ReverseMap()
            //    .ForMember(m => m.Color, m => m.Ignore())
            //    .ForMember(m => m.Category, m => m.Ignore());
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<ApplicationUserIdentity, RegisterDTO>().ReverseMap();
            CreateMap<Product, ProductDTO>().ReverseMap();


        }
    }
}
