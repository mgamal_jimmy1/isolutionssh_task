﻿using AutoMapper;
using BL.Bases;
using BL.DTOs;
using BL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.AppServices
{
    public class UserAppService<TContext> : AppServiceBase<TContext> where TContext : DbContext
    {
        public UserAppService(IUnitOfWork<TContext> theUnitOfWork, IMapper mapper) : base(theUnitOfWork, mapper)
        {
        }
        public bool Create(User user)
        {
            if (user == null)
                throw new ArgumentNullException();
            bool result = false;
            if (TheUnitOfWork.User.Insert(user))
            {
                result = TheUnitOfWork.Commit() > new int();
            }
            return result;
        }
    }
}
