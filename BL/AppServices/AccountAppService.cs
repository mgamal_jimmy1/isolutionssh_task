﻿using BL.Bases;
using BL.Interfaces;
using BL.StaticClasses;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace BL.AppServices
{
    public class AccountAppService<TContext> : AppServiceBase<TContext> where TContext: DbContext
    {
        IConfiguration _configuration;
        UserAppService<EcContext> _userAppService;

        public AccountAppService(IUnitOfWork<TContext> theUnitOfWork,IConfiguration configuration,
             IMapper mapper, UserAppService<EcContext> userAppService) : base(theUnitOfWork, mapper)
        {
            this._configuration = configuration;
            _userAppService = userAppService;
        }
        //public List<RegisterViewodel> GetAllAccounts()
        //{
        //    return Mapper.Map<List<RegisterViewodel>>(TheUnitOfWork.Account.GetAllAccounts().Where(ac => ac.isDeleted == false));
        //}
        //public RegisterDTO GetAccountById(string id)
        //{
        //    if (id == null)
        //        throw new ArgumentNullException();
        //    return Mapper.Map<RegisterViewodel>(TheUnitOfWork.Account.GetAccountById(id));

        //}

        //public bool DeleteAccount(string id)
        //{
        //    if (id == null)
        //        throw new ArgumentNullException();
        //    bool result = false;
        //    ApplicationUserIdentity user = TheUnitOfWork.Account.GetAccountById(id);
        //    user.isDeleted = true;
        //    TheUnitOfWork.Account.Update(user);
        //    result = TheUnitOfWork.Commit() > new int();

        //    return result;
        //}
        public async Task<ApplicationUserIdentity> Find(string name, string password)
        {
            ApplicationUserIdentity user = await TheUnitOfWork.Account.Find(name, password);

            if (user != null)
                return user;
            return null;
        }
        public async Task<ApplicationUserIdentity> FindByName(string userName)
        {
            ApplicationUserIdentity user = await TheUnitOfWork.Account.FindByName(userName);

            if (user != null)
                return user;
            return null;
        }
        public async Task<ApplicationUserIdentity> FindByEmail(string email)
        {
            ApplicationUserIdentity user = await TheUnitOfWork.Account.FindByEmail(email);

            if (user != null)
                return user;
            return null;
        }
        public async Task<IdentityResult> Register(RegisterDTO registerDTO)
        {
            bool isExist = await checkUserNameExist(registerDTO.UserName);
            if(isExist)
                return IdentityResult.Failed(new IdentityError
                { Code = "error", Description = "user name already exist" });

            ApplicationUserIdentity identityUser = Mapper.Map<RegisterDTO, ApplicationUserIdentity>(registerDTO);
            var result = await TheUnitOfWork.Account.Register(identityUser);
            TheUnitOfWork.Commit();
            if (result.Succeeded)
            {
                User user = new User()
                {
                    ID = identityUser.Id,
                    FullName = registerDTO.FullName
                };
                _userAppService.Create(user);
            }
            return result;
        }
        public async Task<IdentityResult> AssignToRole(string userid, string rolename)
        {
            if (userid == null || rolename == null)
                return null;
            return await TheUnitOfWork.Account.AssignToRole(userid, rolename);
        }
        public async Task<bool> UpdatePassword(string userID, string newPassword)
        {
            //    ApplicationUserIdentity identityUser = TheUnitOfWork.Account.FindById(user.Id);

            //    Mapper.Map(user, identityUser);

            //    return TheUnitOfWork.Account.UpdateAccount(identityUser);


            ApplicationUserIdentity identityUser = await TheUnitOfWork.Account.FindById(userID);
            identityUser.PasswordHash = newPassword;
            return await TheUnitOfWork.Account.updatePassword(identityUser);

        }
        public async Task<bool> checkUserNameExist(string userName)
        {
            var user = await TheUnitOfWork.Account.FindByName(userName);
            return user == null ? false : true;
        }
        public async Task<IEnumerable<string>> GetUserRoles (ApplicationUserIdentity user)
        {
            return await TheUnitOfWork.Account.GetUserRoles(user);
        }
       public async Task<dynamic> CreateToken(ApplicationUserIdentity user)
        {
            var userRoles = await GetUserRoles(user);

            var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim("role",userRoles.FirstOrDefault()),
                   new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

            foreach (var userRole in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, userRole));
            }

            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddHours(3),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo
            } ;

           
        }

        public async Task CreateFirstAdmin()
        {
            var firstAdmin = new RegisterDTO()
            {
                Id = null,
                Email = "test@gmail.com",
                UserName = "admin",
                PasswordHash = "@Admin12345",
                ConfirmPassword = "@Admin12345",
                FullName = "First admin"

            };
            Register(firstAdmin).Wait();
            ApplicationUserIdentity foundedAdmin = await FindByName(firstAdmin.UserName);
            if (foundedAdmin != null)
                AssignToRole(foundedAdmin.Id, UserRoles.Admin).Wait();
        }
        public async Task<string> GeneratePasswordResetTokenAsync(ApplicationUserIdentity user)
        {
            return await TheUnitOfWork.Account.GeneratePasswordResetTokenAsync(user);
        }
        public async Task<IdentityResult> ResetPasswordAsync(ApplicationUserIdentity user, string token, string password)
        {
            return await TheUnitOfWork.Account.ResetPasswordAsync(user, token, password);
        }

    }
}
