﻿using BL.Bases;
using BL.Interfaces;
using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using BL.DTOs;

namespace BL.AppServices
{
    public class OrderAppService<TContext> : AppServiceBase<TContext> where TContext:DbContext
    {
        public OrderAppService(IUnitOfWork<TContext> theUnitOfWork, IMapper mapper) : base(theUnitOfWork, mapper)
        {

        }
        #region CURD
        public bool Insert(OrderDTO orderViewModel)
        {
            if (orderViewModel == null)
                throw new ArgumentNullException();
            if (orderViewModel.UserId == null || orderViewModel.UserId == string.Empty)
                throw new ArgumentException();
            bool result = false;
            var order = Mapper.Map<Order>(orderViewModel);
            if (TheUnitOfWork.Order.Insert(order))
            {
                result = TheUnitOfWork.Commit() > new int();
            }
            return result;
        }

        public bool DeleteOrder(int id)
        {
            if (id <= 0)
                throw new ArgumentOutOfRangeException();

            bool result = false;

            TheUnitOfWork.Order.Delete(id);
            result = TheUnitOfWork.Commit() > new int();

            return result;
        }
        #endregion

        public IEnumerable<OrderDTO> GetOrdersForSpeceficUser(string userID)
        {
            return Mapper.Map<List<OrderDTO>>(TheUnitOfWork.Order.GetOrdersForSpeceficUser(userID));
        }

    }
}
