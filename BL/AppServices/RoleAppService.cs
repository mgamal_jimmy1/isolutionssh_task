﻿using BL.Bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Microsoft.AspNetCore.Identity;
using BL.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using BL.DTOs;

namespace BL.AppServices
{
    public class RoleAppService<TContext> : AppServiceBase<TContext> where TContext:DbContext
    {
        public RoleAppService(IUnitOfWork<TContext> theUnitOfWork, IMapper mapper) : base(theUnitOfWork, mapper)
        {

        }
        public async Task CreateRoles()
        {
           await TheUnitOfWork.Role.CreateRoles();
        }
        public RoleDTO GetRoleById(string id)
        {
            if (id == null || id == "")
                throw new ArgumentNullException();

            return Mapper.Map<RoleDTO>(TheUnitOfWork.Role.GetRoleByID(id));
        }
        public IdentityResult Create(string rolename)
        {
            return TheUnitOfWork.Role.Create(rolename);
        }
        public async Task <IdentityResult> Update(RoleDTO roleViewModel)
        {
            if (roleViewModel == null)
                throw new ArgumentNullException();
            if (roleViewModel.Id == null || roleViewModel.Id == string.Empty)
                throw new ArgumentException();

            var role = Mapper.Map<IdentityRole>(roleViewModel);
            return await TheUnitOfWork.Role.UpdateRole(role);
        }
        public bool DeleteRole(string id)
        {
            if (id == null || id == "")
                throw new ArgumentNullException();
            bool result = false;

            TheUnitOfWork.Role.DeleteRole(id);
            result = TheUnitOfWork.Commit() > new int();

            return result;
        }
    }
}
