﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Bases;
using BL.Interfaces;
using DAL.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using BL.DTOs;

namespace BL.AppServices
{
    public class ProductAppService<TContext>: AppServiceBase<TContext> where TContext:DbContext
    {
        public ProductAppService(IUnitOfWork<TContext> theUnitOfWork, IMapper mapper) : base(theUnitOfWork, mapper)
        {
        }
        public IEnumerable<ProductDTO> GetAllProduct()
        {
            IEnumerable<Product> allProducts = TheUnitOfWork.Product.GetAllProduct();
            return Mapper.Map<IEnumerable<ProductDTO>>(allProducts);
        }

        public ProductDTO GetProduct(int id)
        {
            return Mapper.Map<ProductDTO>(TheUnitOfWork.Product.GetProductById(id));
        }
        public bool insert(ProductDTO productDTO)
        {
            if (productDTO == null)
                throw new ArgumentNullException();
            bool result = false;
            var product = Mapper.Map<Product>(productDTO);
            if (TheUnitOfWork.Product.Insert(product))
            {
                result = TheUnitOfWork.Commit() > new int();
            }
            return result;
        }


        public bool UpdateProduct(ProductDTO productDTO)
        {
            var productFromDb= TheUnitOfWork.Product.GetById(productDTO.ID);
            //if(productViewModel.Image == null)
            //    productViewModel.Image = productFromDb.Image;
            Mapper.Map(productDTO, productFromDb);
            TheUnitOfWork.Product.Update(productFromDb);
            TheUnitOfWork.Commit();

            return true;
        }
        public bool DeleteProduct(int id)
        {
            bool result = false;

            TheUnitOfWork.Product.Delete(id);
            result = TheUnitOfWork.Commit() > new int();

            return result;
        }
        public bool CheckProductExists(ProductDTO productDTO)
        {
            Product product = Mapper.Map<Product>(productDTO);
            return TheUnitOfWork.Product.CheckProductExists(product);
        }

        #region pagination
        public int CountEntity()
        {
            return TheUnitOfWork.Product.CountProducts();
        }
        public IEnumerable<ProductDTO> GetPageRecords(int pageSize, int pageNumber)
        {
            var products = Mapper.Map<IEnumerable<ProductDTO>>(TheUnitOfWork.Product.GetPageRecords(pageSize, pageNumber));
            return products;
        }
        #endregion
    }
}
