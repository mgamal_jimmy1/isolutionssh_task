﻿using BL.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface IUnitOfWork<TContext> : IDisposable where TContext : DbContext
    {
        #region Methode
        int Commit();
        #endregion

        AccountRepository<TContext> Account { get; }
        UserRepository<TContext> User { get; }
        RoleRepository<TContext> Role { get; }
        ProductRepository<TContext> Product { get; }
        OrderRepository<TContext> Order { get; }

    }
}
