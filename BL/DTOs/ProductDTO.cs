﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.DTOs
{
    public class ProductDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string NameAr { get; set; }
        public string Image { get; set; }
    }
}
