﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.DTOs
{
    public class OrderDTO
    {
        public int ID { get; set; }
        public string UserId { get; set; }
        public int ProductId { get; set; }
    }
}
