﻿using BL.AppServices;
using BL.DTOs;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        OrderAppService<EcContext> _orderAppService;
        ProductAppService<EcContext> _productAppService;
        IHttpContextAccessor _httpContextAccessor;
        public OrderController(OrderAppService<EcContext> orderAppService,
            ProductAppService<EcContext> productAppService, IHttpContextAccessor httpContextAccessor)
        {
            this._orderAppService = orderAppService;
            this._productAppService = productAppService;
            this._httpContextAccessor = httpContextAccessor;
        }


        [HttpPost]
        public IActionResult makeOrder(OrderDTO orderDTO)
        {
            _orderAppService.Insert(orderDTO);
        
            return Ok("order made");
        }
        [HttpGet("{userID}")]
        public IActionResult GetOrdersByPageForSpecficUser(string userID)
        {
            return Ok(_orderAppService.GetOrdersForSpeceficUser(userID));
        }
    }
}
