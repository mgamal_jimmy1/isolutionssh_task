﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.AppServices;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using DAL;
using BL.StaticClasses;
using Microsoft.AspNetCore.Authorization;
using BL.DTOs;
using Microsoft.AspNetCore.Identity;
using EmailService;
using Api.HelpClasses;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private AccountAppService<ApplicationDBContext> _accountAppService;
        private RoleAppService<ApplicationDBContext> _roleAppService;
        IHttpContextAccessor _httpContextAccessor;
        EmailSender _emailSender;
        public AccountController(
            AccountAppService<ApplicationDBContext> accountAppService,
            RoleAppService<ApplicationDBContext> roleAppService,
            IHttpContextAccessor httpContextAccessor, EmailSender emailSender)
        {
            this._accountAppService = accountAppService;
            this._roleAppService = roleAppService;
            this._httpContextAccessor = httpContextAccessor;
            _emailSender = emailSender;
        }

        [HttpPost("/Register")]
        public async Task<IActionResult> RegisterUser([FromBody] RegisterDTO model)
        {

            return await Register(model, UserRoles.User);

        }
        private async Task<IActionResult> Register(RegisterDTO registerDTO, string roleName)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _accountAppService.Register(registerDTO);

            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError,
                     result.Errors.FirstOrDefault().Description);

            ApplicationUserIdentity identityUser = await _accountAppService.Find(registerDTO.UserName, registerDTO.PasswordHash);

            //create roles
            await _roleAppService.CreateRoles();
            await _accountAppService.AssignToRole(identityUser.Id, roleName);
            return Ok(new Response { Status="200", Message="user created" } );
        }
        [HttpPost("/Login")]
        public async Task<IActionResult> Login([FromBody] LoginDTO model)
        {
            var user = await _accountAppService.Find(model.UserName, model.PasswordHash);
            if (user != null)
            {
                dynamic token = await _accountAppService.CreateToken(user);

                return Ok(token);
            }
            return Unauthorized();
        }
        [HttpGet("/forgotPassword/{email}")]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _accountAppService.FindByEmail(email);

            if (user == null)
                return BadRequest("this email doesn't exist");

            var token = await _accountAppService.GeneratePasswordResetTokenAsync(user);
            var callback = $"http://localhost:4200/resetPassword?token={token}&&email={user.Email}";

            var message = new Message(user.Email, "Reset password token", callback);
            _emailSender.DisableCertificateValidation();
            await _emailSender.SendEmailAsync(message);

            return Ok("reset password link sent to email");
        }
        [HttpPost("/resetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordDTO resetPasswordDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _accountAppService.FindByEmail(resetPasswordDTO.Email);

            if (user == null)
                return BadRequest("this email doesn't exist");
            var correctToken = resetPasswordDTO.Token.Replace(" ", "+");
            var resetPassResult = await _accountAppService.ResetPasswordAsync(user, correctToken, resetPasswordDTO.PasswordHash);
            if (!resetPassResult.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError,
                         resetPassResult.Errors.FirstOrDefault().Description);

            return Ok("password reset successfully");
        }
    }
}
